const board_border = 'black';
const board_background = "darkblue";
const snake_color = 'lightgreen';
const snake_border = 'darkblue';

let snake = [
    { x: 200, y: 200, speed_x: 5, speed_y: 5 },
    { x: 180, y: 200, speed_x: 5, speed_y: 5 },
    { x: 160, y: 200, speed_x: 5, speed_y: 5 }
]

let changing_direction = false;
let dx = 20;
let dy = 0;
let cake_x;
let cake_y;
let cookie_x;
let cookie_y;

const canvas = document.getElementById("gameCanvas");
const context = canvas.getContext("2d");

// Start game
let speed = 100;
main();
gen_cake();

sleep(8000).then(() => { gen_cookie(); })
let modal = document.querySelector(".modal")
// main function called repeatedly to keep the game running
function main() {

    if (has_game_ended()) {
        // alert('Game Over'+'\n'+'votre score est :'+score)
        modal.style.display = "block";
        // modal.classList.add("show");
        return;
    }

    changing_direction = false;
    setTimeout(function onTick() {
        clearCanvas();
        drawCake();
        drawCookie();
        moveSnake();
        drawSnake();
        // Repeat
        main();
    }, speed)
}


function clearCanvas() {
    context.fillStyle = board_background;
    context.strokestyle = board_border;
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.strokeRect(0, 0, canvas.width, canvas.height);
}

function drawSnake() {
    snake.forEach(drawSnakePart);
}

function drawCake() {
    var cake = new Image();
    cake.onload = function () {
        context.drawImage(cake, cake_x, cake_y, 20, 20);
    };
    cake.src = 'cake.png';
}
function drawCookie() {
    var cookie = new Image();
    cookie.onload = function () {
        context.drawImage(cookie, cookie_x, cookie_y, 20, 20);
    };
    cookie.src = 'cookie.png';
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function drawSnakePart(snakePart) {
    context.fillStyle = snake_color;
    context.strokestyle = snake_border;
    context.fillRect(snakePart.x, snakePart.y, 20, 20);
    context.strokeRect(snakePart.x, snakePart.y, 20, 20);
}


function has_game_ended() {
    for (let i = 2; i < snake.length; i++) {
        const has_collided = snake[i].x === snake[0].x && snake[i].y === snake[0].y
        if (has_collided)
            return true
    }
    const hitLeftWall = snake[0].x < 0;
    const hitRightWall = snake[0].x > canvas.width - 20;
    const hitToptWall = snake[0].y < 0;
    const hitBottomWall = snake[0].y > canvas.height - 20;

    return hitLeftWall || hitRightWall || hitToptWall || hitBottomWall
}
function random_cake(min, max) {
    return Math.round((Math.random() * (max - min) + min) / 20) * 20;
}

function random_cookie(min, max) {
    return Math.round((Math.random() * (max - min) + min) / 20) * 20;
}

function gen_cake() {
    cake_x = random_cake(0, canvas.width - 20);
    cake_y = random_cake(0, canvas.height - 20);
    snake.forEach(function has_snake_eaten_cake(part) {
        const has_eaten = part.x == cake_x && part.y == cake_y;
        if (has_eaten) gen_cake();
    });
}

function gen_cookie() {
    cookie_x = random_cookie(0, canvas.width - 20);
    cookie_y = random_cookie(0, canvas.height - 20);
    snake.forEach(function has_snake_eaten_cookie(part) {
        const has_eaten = part.x == cookie_x && part.y == cookie_y;
        if (has_eaten) gen_cookie();
    });
}
document.addEventListener("keydown", (event) => {
    const LEFT_KEY = 37;
    const RIGHT_KEY = 39;
    const UP_KEY = 38;
    const DOWN_KEY = 40;

    const keyPressed = event.keyCode;
    const goingUp = dy === -20;
    const goingDown = dy === 20;
    const goingRight = dx === 20;
    const goingLeft = dx === -20;

    if (keyPressed === LEFT_KEY && !goingRight) {
        dx = -20;
        dy = 0;
    }

    if (keyPressed === UP_KEY && !goingDown) {
        dx = 0;
        dy = -20;
    }

    if (keyPressed === RIGHT_KEY && !goingLeft) {
        dx = 20;
        dy = 0;
    }

    if (keyPressed === DOWN_KEY && !goingUp) {
        dx = 0;
        dy = 20;
    }
})
let score = 0
function moveSnake() {
    // Create the new Snake's head
    const head = { x: snake[0].x + dx, y: snake[0].y + dy };
    // Add the new head to the beginning of snake body
    snake.unshift(head);
    if (snake[0].x === cake_x && snake[0].y === cake_y) {
        // Increase score
        score += 100;
        speed -= 5;
        // Display score on screen
        document.getElementById('score').innerHTML = score;
        // Generate new cake location
        gen_cake();
    } else if (snake[0].x === cookie_x && snake[0].y === cookie_y) {
        // Increase score
        score += 200;
        speed += 5;
        // Display score on screen
        document.getElementById('score').innerHTML = score;
        // Generate new cake location
        cookie_x = 10000;
        cookie_y = 10000;
        sleep(8000).then(() => { gen_cookie(); })
    } else {
        // Remove the last part of snake body
        snake.pop();
    }
}